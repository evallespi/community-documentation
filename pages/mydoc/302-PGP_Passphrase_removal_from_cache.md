---
title: Remove PGP Passphrase from Cache
keywords: PGP, passphrase, password, cache, Enigmail, command line, gpg-agent, GPG Suite, email security
last_updated: January 11, 2019
tags: [secure_communications, articles]
summary: "A user reports that their PGP client encrypts or decrypts emails without requesting a passphrase."
sidebar: mydoc_sidebar
permalink: 302-PGP_Passphrase_Removal_from_Cache.html
folder: mydoc
conf: Public
lang: en
---


# Remove PGP Passphrase from Cache
## Troubleshoot the non requirement of passphrase for PGP related operations

### Problem

The passphrase of the key could be cached. If the device is accessed, the passphrase could be compromised alongside the PGP private key. An adversary who got access to the client's device and found this information would be able to decrypt the passphrase and read all available PGP-encrypted communications.  


* * *


### Solution

1. **First scenario: The key pair has been generated without a passphrase.**

    The first thing we should check with the client is whether they have ever been prompted to enter a passphrase to decrypt their email, as the key pair could have been generated without a passphrase. 

    If this is the case, explain how it is not secure to have a PGP key without a passphrase and help them - based on the PGP implementation they use - to set a strong passphrase. Check below the different ways to change or set a passphrase for an existing PGP key.

    - [Command line](https://www.cyberciti.biz/faq/linux-unix-gpg-change-passphrase-command/)
    - [Using Enigmail](https://www.enigmail.net/documentation/Key_Management#Changing_the_passphrase)
    - Unavailable on Mailvelope, but there are [other options](https://www.mailvelope.com/en/faq#change_pwd)
- [GPG Suite on Mac](https://gpgtools.tenderapp.com/discussions/problems/28388-changing-passphrase-on-mavericks)

Before adding a passphrase, inform the client on how to generate a strong passphrase, sending them [this guide](https://ssd.eff.org/en/module/creating-strong-passwords). Also consider mentioning [the diceware method](https://www.eff.org/dice).

2. **Second scenario: Enigmail is set to remember the passphrase.** 

    You can check if this is the case by going to: Options menu -&gt; Enigmail -&gt; Preferences -&gt; General (or Basic).

    In this tab, check if the box "Never ask for any passphrase" is checked. if so, ask the client to uncheck it. In the same tab, make sure the passphrase is remembered for 30 to 60 minutes at most. 

3. **Third scenario: The passphrase is cached by the system**

    - For **Mac** users, check in GPGSuite if this is the case by navigating to: System Preferences -&gt; GPG Suite.

        If the the box "Store in macOS keychain" is selected, disable it and hit the button "Remove".

        In the same tab, make sure that the passphrase is remembered for 30 to 60 minutes at most. More details can be found [here](https://gpgtools.tenderapp.com/kb/faq/password-management).

    - **Linux** users just need to restart `gpg-agent`. From a terminal: 

        ```
        $ gpgconf --kill gpg-agent
        ```

        After this operation, it is not necessary to manually restart `gpg-agent`: it will automatically restart when it is needed.

        Also check the configuration file under `~/.gnupg/gpg-agent.conf`. Make sure that the value for `default-cache-ttl` is not too high, as this indicates how long `gpg-agent` will remember the passphrase. Consider that the time is calculated in seconds (indicatively, this value will be 1800 for 30 minutes). 

    - On **Windows**, `gpg-agent` needs to be restarted:

        From the terminal:

        ```shell
        $ echo "RELOADAGENT" | gpg-connect-agent
        ```

        **Note**: This solution has not been tested and was found [online](https://marc.info/?l=gpg4win-users-en&amp;m=143801504129651&amp;w=2).


* * *


### Comments

For the most sensitive situations, if the passphrase is found to be cached for a long time, it is recommended to consider the key not safe, as even changing the passphrase would not help in case the key has been already compromised. In such cases, we should revoke the key and generate a new one for the client/staff member.

The delay between 30 and 60 mins is in compliance with Access Now's PGP policy. However, for some threat models the delay might have to be shorter. 

#### References

- The GNU Privacy Handbook - [Protecting your private key](https://gnupg.org/gph/en/manual.html#AEN513)


* * *


### Related Articles
