---
title: PGP - Extend Expiration Date
keywords: email, PGP, key management, expiration date, Enigmail, Thunderbird, email security
last_updated: November 6, 2018
tags: [secure_communications, articles]
summary: "A client's PGP key has expired and they wish to renovate it. In some cases, without the client's warning, we may initiate the process to extend the expiration date of a client's PGP key because we have received an alert about the key expiration, or we have checked the expiration date manually and found out that it will expire soon."
sidebar: mydoc_sidebar
permalink: 37-PGP_Extend_Expiration.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Extend Expiration Date
## How to extend the expiration date of a PGP key

### Problem


A client's PGP key has expired or will expire soon. If the client wants to keep using that PGP key, they should extend the expiration date of the key and need instructions on how to do it.


* * *



### Solution

1. Send an email to the client to explain that their key expiration date needs to be extended and how to do it. Please base this email on the template in [Article #36: PGP - Extend Expiration Date - Email](36-PGP_Extend_Expiration_Email.html).

2. Wait for the client's confirmation that they have updated their key and uploaded it on the key servers.

3. Check that the change has been completed successfully. Ensure the expiration dates of both the **primary key** and **subkey** are updated.

4. Update the relevant keyrings with the new key configuration.
5. Send an email to the client based on [Article #40: PGP - Extend Expiration Date - Confirmation Email](40-PGP_Extend_Expiration_Confirmation_Email.html) to close the case.


* * *


### Comments



* * *


### Related Articles

- [Article #36: PGP - Extend Expiration Date - Email](36-PGP_Extend_Expiration_Email.html)
- [Article #40: PGP - Extend Expiration Date - Confirmation Email](40-PGP_Extend_Expiration_Confirmation_Email.html)
- Article #38
