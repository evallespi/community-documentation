---
title: Initial Reply for Clients You Suspect Are Paranoid
keywords: helpline procedures templates, paranoia, psychological security, client in distress, initial reply
last_updated: April 29, 2019
tags: [helpline_procedures_templates, templates]
summary: "First response to clients who we suspect are suffering from paranoia"
sidebar: mydoc_sidebar
permalink: 357-paranoia_template.html
folder: mydoc
conf: Public
ref: paranoia_template
lang: en
---


# Initial Reply for Clients You Suspect Are Paranoid
## First response to clients who we suspect are suffering from paranoia

### Body

Dear $ClientName,

Thank you for contacting the Access Now Digital Security Helpline
(https://www.accessnow.org/help). My name is $IHName, and I am
here to support you into understanding the problem that you are
experiencing and to figure out what you want to do next.

We have received your initial request and I am working on it. To
better support and respond to your needs, we need to ask a few
questions and keep track of what has happened to follow up.

First, I am sorry you are going through this. I want to help as much as I can.  Before I am able to help, I need some more information.

Please take your time to answer these questions:

- Would it be appropriate if we set up a call with you, either video
or just audio?
- Who do you think is trying to harm you? What is their motive? Why have they chosen you? Is there something you have that they need? What kind of threats or harm are occurring?

Before we talk again I want to ask you to help me out by keeping
record of what happens, saving important information and find a
trusted friend who can comfort you and give you a hand managing
the incident you are experiencing.

Please reply to this message including “[accessnow #ID]” in the
subject line and your answer to my questions

Kind regards and stay strong,

$IHName


* * *


### Comments

*You may have some of this information from the initial contact from the client.  If you already have it, repeat it back to them by writing, “I want to make sure I understand what is happening,” then repeat back the answers to the questions.*


* * *


### Related Articles

- [Article #356: Protocol for a Client We Suspect Is Paranoid](356-paranoia_protocol.html)
