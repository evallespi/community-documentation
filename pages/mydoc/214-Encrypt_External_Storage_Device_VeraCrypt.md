---
title: Encrypt an External Storage Device with VeraCrypt
keywords: device encryption, Veracrypt, backup, external storage device
last_updated: September 2, 2019
tags: [devices_data_security, articles]
summary: "A client needs to store sensitive data in an external storage device, like a USB stick, a memory card, or an external hard drive. The storage device risks being accessed by unauthorized parties, so it should be fully encrypted to protect the stored data."
sidebar: mydoc_sidebar
permalink: 214-Encrypt_External_Storage_Device_VeraCrypt.html
folder: mydoc
conf: Public
ref: Encrypt_External_Storage_Device_VeraCrypt
lang: en
---


# Encrypt an External Storage Device with VeraCrypt
## Steps to encrypt a USB drive or other external storage device using VeraCrypt

### Problem

- Sensitive information is being stored in an unencrypted storage device.
- Sensitive files need to be protected in case a computer or storage device is stolen or lost or there is a break-in in the client's office.
- Backups are currently stored with no protection or encryption.
- A storage device containing sensitive data needs to be sent by snail mail.


* * *


### Solution

The tool we recommend for encrypting entire storage devices is [VeraCrypt](https://www.veracrypt.fr). It can be installed in Windows, macOS, and Linux, and it can be downloaded [here](https://www.veracrypt.fr/en/Downloads.html).

*If the client is a high-risk user, we should strongly recommend they verify the installer against its PGP signature (available in the downloads page next to each installer) before launching the installation, guiding them step by step with instructions on how to do it.*


#### Encrypting the device

- Install and launch VeraCrypt.
- Click the "Create Volume" button.
- On Windows, select "Encrypt a non-system partition/drive". On Linux and macOS, select "Create a volume within a partition/drive". Click Next.
- Select "Standard VeraCrypt volume" and click Next.
- Click "Select Device" locate your USB drive from the list displayed (you should select the entry with the letter corresponding to the drive if you find multiple entries for the USB). Click Next.
- In the "Encryption Options" windows, you can leave the default settings and click "Next".
- Enter a strong password in the "Password" field and re-type it in the "Confirm password" field. Save this password in a secure place: you will need this to decrypt your device. Click Next.
- VeraCrypt will encrypt your device now. Move your mouse as randomly as possible within the Volume Creation Wizard window at least until the randomness indicator becomes green. The longer you move the mouse, the better (moving the mouse for at least 30 seconds is recommended). This significantly increases the cryptographic strength of the encryption keys (which increases security).
- Click "Format". Depending on the size of the volume, the volume creation may take a long time. After it finishes, a dialog box will appear to confirm that the device has been encrypted. Click "OK" and then "Exit". The Wizard window should disappear.

#### Decrypting the device

A device encrypted with VeraCrypt can only be decrypted in a machine that has VeraCrypt installed in it.

- Connect the device to the machine and launch VeraCrypt.
- Click the "Select Device" button.
- A box with a list of devices will be displayed. Select your encrypted drive and click OK.
- In the main menu, select a free drive slot.
- Click the "Mount" button and enter the password you typed when encrypting the device for the first time.


* * *


### Comments

Also see [VeraCrypt's official documentation](https://www.veracrypt.fr/en/Documentation.html).

If the client just needs to encrypt a folder within their hard drive, please see [Article #210: File Encryption with Veracrypt or GPG](210-File_Encryption_VeraCrypt_GPG.html).

If the client needs to fully encrypt the hard drive of their computer, see [Article #166: FAQ - Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html).


* * *

### Related Articles

- [Article #210: File Encryption with Veracrypt or GPG](210-File_Encryption_VeraCrypt_GPG.html)
- [Article #166: FAQ - Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html)
