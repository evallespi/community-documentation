---
title: Disable C&amp;C Server - Email to Client
keywords: C&amp;C server, malware, email templates, malicious website
last_updated: July 20, 2018
tags: [vulnerabilities_malware_templates, templates]
summary: "Email to ask the client or partner for permission to share malware indicators with the digital security community after a C&amp;C server has been disabled"
sidebar: mydoc_sidebar
permalink: 261-Disable_Malicious_Server_client.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server_client
lang: en
---


# Disable C&amp;C Server - Email to Client
## Template for ask the client for permission to share the malware indicators with the digital security community

### Body

Dear [Client's name],

My name is [IH's name]. I am part of Access Now's Digital Security Helpline team - https://accessnow.org/help. I'm writing to let you know that we are in process of dealing with your request and we need some information that will help resolve the issue. 

I am asking for your permission to share indicators about the malware and Command &amp; Control server associated with it within our community. Sharing the malware and indicators will make the malware less effective and protect others who may be targeted.

We will be looking forward to your reply to move forward on this case.

Thank you,

[IH's name]


* * *


### Related Articles

- [Article #219: Targeted Malware: Disable Malicious C&amp;C Server](219-Targeted_Malware_Disable_Malicious_Server.html)
