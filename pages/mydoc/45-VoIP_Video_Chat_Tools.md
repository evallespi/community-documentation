---
title: Recommendations on VoIP/video Chat Tools
keywords: VoIP, video chat, voice calls, video calls
last_updated: April 2, 2020
tags: [secure_communications, articles]
summary: "A client is looking for a secure alternative to call people; an at risk user is concerned about making calls which can be monitored or intercepted; a client uses Skype, Google Hangouts, or other commercial services for sensitive voice/video chat"
sidebar: mydoc_sidebar
permalink: 45-VoIP_Video_Chat_Tools.html
folder: mydoc
conf: Public
lang: en
---


# Recommendations on VoIP/video Chat Tools
## Secure solutions for voice/video calls

### Problem

- Secure communications via email may not be possible or suitable in a specific situation.
 
- Telecommunications companies may try to monitor or intercept calls going through their network.

- Popular tools like Skype or Zoom are not advisable solutions for human right defenders at risk.

- Depending on their threat model, other commercial services like Google Hangouts can also be contraindicated.


* * *


### Solution

#### 1:1 video calls

For end-to-end encrypted 1:1 voice and video calls, we can recommend:

- **Wire** - an open source desktop and mobile app for end-to-end encrypted messaging (both 1:1 and group) and calls (both 1:1 and groups with fewer than 10 participants – see further information about the latter in the section on multi-party video calls below) that doesn't need a telephone number to sign up.
    - From the Wire Support website: [Audio and video calls, call settings](https://support.wire.com/hc/en-us/categories/200207424-Conferencing)

- **Signal** - an open source desktop and mobile app for end-to-end encrypted messaging (both 1:1 and group) and calls (only 1:1).
    - From the Signal Support website: [How to start a voice or video call](https://support.signal.org/hc/en-us/articles/360007060492-Voice-or-Video-Calling)
    - Further useful information on the Signal Support website: [How to install Signal on Android, iOS, desktop](https://support.signal.org/hc/en-us/articles/360008216551-Installing-Signal)

#### Multi-party video calls

Currently there is no solution for end-to-end encrypted group calls. 

Since group video calls are not end-to-end encrypted, their security depends on TLS/HTTPS being properly implemented and the entity that is hosting the service, so in choosing a platform the user is trusting that platform with the confidentiality of their conversations. If they do not feel comfortable trusting another party with access to this information, then they need to explore self-hosting and finding a platform they do trust.

- **Jitsi Meet** is a video conference browser-based option. It was developed by the same team of Jitsi, the desktop voice and messaging client. The main difference is you don't need to install a client for your computer, since you will connect using your browser to a Jitsi Meet server. You can try this solution simply by going to [the Jitsi Meet official website](https://meet.jit.si).

    Since this service is not end-to-end encrypted, we should recommend an instance of Jitsi Meet hosted by a partner who you trust. Some examples could include:

    - [https://meet.mayfirst.org](https://meet.mayfirst.org)
    - [https://vc.autistici.org](https://vc.autistici.org)
    - [https://calls.disroot.org](https://calls.disroot.org)
    - [https://meet.guifi.net](https://meet.guifi.net)
    - [https://meet.systemli.org/](https://meet.systemli.org/)
    - [https://meet.greenhost.net](https://meet.greenhost.net)

    If the client has system administrator capacity within their organization, they can also choose to self-host a Jitsi Meet instance. Instructions for installing Jitsi Meet in a server can be found [here](https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md) or they can use the [public Ansible role by the tech collective Systemli](https://github.com/systemli/ansible-role-jitsi-meet). [Greenhost's](https://greenhost.nl/) [Eclips.is platform](https://eclips.is) has a pre-configured setup to launch a Jitsi Meet instance on their cloud as well.

- **Wire** - For video calls with fewer than 10 participants, we can recommend Wire. Group video calls on Wire *are not encrypted*, but the audio and video quality is pretty good and stable. Participants can connect through the mobile or desktop app.
    - From the Wire Support website: [Group calls](https://support.wire.com/hc/en-us/sections/360000581318-Group-calls)

- Other server-trusting proprietary solutions for multi-party calls include [appear.in](https://appear.in/), [talky.io](https://talky.io/), [GoToMeeting](https://www.gotomeeting.com), and [Google Hangouts Meet](https://meet.google.com).

#### Multi-party video learning platforms

If the client is interested in hosting remote video learning, they can consider the open-source video platform [BigBlueButton](https://bigbluebutton.org/). There are some [commercial providers](https://bigbluebutton.org/commercial-support/) that also offer hosting support if the client doesn't have the technical capacity. 

#### Voice calls

If video is not strictly necessary, and bandwidth is low, it is worth proposing multi-party audio calls, for example with **Mumble**, an open source, low-latency, high quality voice chat software that can be self-hosted in the client's server.

- To use Mumble, the participants in the conversation need to install a client:
    - [Mumble for Windows, Mac, and Linux](https://wiki.mumble.info/wiki/Main_Page)
    - Plumble for Android: [Google Play](https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient.free) - [F-Droid](https://f-droid.org/packages/com.morlunk.mumbleclient/)
    - [Mumblefy for iOS](https://itunes.apple.com/dk/app/mumblefy/id858752232?mt=8)

- Mumble servers managed by trusted parties:
    - kefir.red  (Mexico) - [Instructions in Spanish](https://wiki.kefir.red/Mumble)
    - mumble.contaminati.net (Italy)
    - fala.navemae.xyz (Brazil) - [Instructions in Portuguese](https://wiki.ativismo.org.br/index.php?title=Mumble)
    - mumble.riseup.net (USA)
    - mumble.codigosur.net (Costa Rica)
    - [talk.systemli.org](https://www.systemli.org/en/service/mumble.html) (Germany)

- [Instructions for installing Mumble in a server](https://wiki.mumble.info/wiki/Installing_Mumble)
- If the Mumble conversation needs to be password-protected, a private channel can be created. To create a private channel, right-click when you connect to Mumble and set the name and a password for the channel. When you leave the channel, the logs and conversation disappear.


* * *


### Comments

[Rocket.Chat](https://rocket.chat/) - an open source fully featured Slack clone - has plugins for webrtc videobridging and OTR-encrypted chat conversations. It can be self-hosted as a standalone, or within a sandstorm.io instance, and could be more useful for organizations looking to consolidate Slack-style communications with a voice option.

If we consider other tools for voice and video call, we should use the [Confidentiality-Integrity-Availability Framework](https://en.wikipedia.org/wiki/Information_security) to evaluate them and make sure that they include these features:

* Cross-platform
* End-to-End encrypted whenever possible
* Documented
* Easy to install
* User-friendly
* Containing user authentication
* Open source
* Hosted by trusted entities
