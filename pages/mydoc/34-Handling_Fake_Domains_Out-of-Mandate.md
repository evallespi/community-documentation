---
title: Handling Fake Domains in Out-of-Mandate Cases
keywords: fake domain, out of mandate, commercial
last_updated: March 4, 2019
tags: [fake_domain_mitigation, articles]
summary: "A fake domain has been reported, but after the vetting process, we have found out the client is not part of civil society but is a commercial entity or out of mandate for other reasons."
sidebar: mydoc_sidebar
permalink: 34-Handling_Fake_Domains_Out-of-Mandate.html
folder: mydoc
conf: Public
lang: en
---


# Handling Fake Domains in Out-of-Mandate Cases
## What to do if a fake domain is reported by a client out of mandate

### Problem

Access Now Helpline, as a responsible CERT, is willing to responsibly report fake domain cases with commercial involvement. However, commercial entities and other types of requestors are not under Access Now's mandate, and therefore the amount of resources to handle this type of cases should be as limited as possible.


* * *


### Solution

- Send the requestor an email with instructions on how to report the domain based on the template in [Article #217: Reporting a Domain Name in Out-of-Mandate Cases](217-Reporting_domain_out-of-mandate.html).

#### Optional steps:

- If the domain is a country-code top level domain, contact the local CERT.

- If the domain is a generic top level domain, contact the local CERT of the country where the commercial entity is registered.

- If there is no local CERT, report the malicious activity directly to the registrar responsible for the domain.

A list of CERTs per country can be found [here](http://first.org/members/map).


* * *


### Comments

For cases of fake domains within mandate, please see [Article #342: Handling of Fake Domains](342-Handling_Fake_Domains.html)


* * *

### Related Articles

- [Article #217: Reporting a Domain Name in Out-of-Mandate Cases](217-Reporting_domain_out-of-mandate.html)
- [Article #342: Handling of Fake Domains](342-Handling_Fake_Domains.html)
